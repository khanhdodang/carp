import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.openBrowser('')

WebUI.navigateToUrl('https://the-internet.herokuapp.com/add_remove_elements/')

WebUI.maximizeWindow()

WebUI.verifyElementText(findTestObject('Object Repository/Page_The Internet/h3_AddRemove Elements'), 'Add/Remove Elements')

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_The Internet/button_Add Element'), 0)

WebUI.verifyElementText(findTestObject('Object Repository/Page_The Internet/button_Add Element'), 'Add Element')

WebUI.click(findTestObject('Object Repository/Page_The Internet/button_Add Element'))

WebUI.click(findTestObject('Object Repository/Page_The Internet/button_Delete'))

WebUI.click(findTestObject('Object Repository/Page_The Internet/button_Add Element'))

WebUI.click(findTestObject('Object Repository/Page_The Internet/button_Add Element'))

WebUI.rightClick(findTestObject('Object Repository/Page_The Internet/button_Delete'))

WebUI.click(findTestObject('Object Repository/Page_The Internet/button_Delete'))

WebUI.verifyElementPresent(findTestObject('Object Repository/Page_The Internet/button_Delete'), 0)

WebUI.click(findTestObject('Object Repository/Page_The Internet/button_Add Element'))

WebUI.click(findTestObject('Object Repository/Page_The Internet/button_Delete'))

