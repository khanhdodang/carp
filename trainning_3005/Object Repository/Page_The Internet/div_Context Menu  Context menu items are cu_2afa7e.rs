<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Context Menu  Context menu items are cu_2afa7e</name>
   <tag></tag>
   <elementGuidId>81446a76-69db-4da2-bae0-a7b2f3d14f40</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='content']/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.example</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>eded8f8a-b06b-4ac7-91df-d2ff43b4cc8a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>example</value>
      <webElementGuid>2b100cef-e2ae-4d01-abb6-6f617508ec17</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
  Context Menu
  Context menu items are custom additions that appear in the right-click menu.
  Right-click in the box below to see one called 'the-internet'. When you click it, it will trigger a JavaScript alert.
  
  
</value>
      <webElementGuid>0483167f-0749-4dc8-9408-b8f9bbe8ac55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;content&quot;)/div[@class=&quot;example&quot;]</value>
      <webElementGuid>35af787f-adf1-4776-8300-7a7fc00c4b86</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='content']/div</value>
      <webElementGuid>aec33719-69fb-4c21-8825-03465ccf5d10</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div</value>
      <webElementGuid>8629a1d5-ffe2-49c8-943b-ba09fe05dd45</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = concat(&quot;
  Context Menu
  Context menu items are custom additions that appear in the right-click menu.
  Right-click in the box below to see one called &quot; , &quot;'&quot; , &quot;the-internet&quot; , &quot;'&quot; , &quot;. When you click it, it will trigger a JavaScript alert.
  
  
&quot;) or . = concat(&quot;
  Context Menu
  Context menu items are custom additions that appear in the right-click menu.
  Right-click in the box below to see one called &quot; , &quot;'&quot; , &quot;the-internet&quot; , &quot;'&quot; , &quot;. When you click it, it will trigger a JavaScript alert.
  
  
&quot;))]</value>
      <webElementGuid>9361d75f-817b-438e-bb6b-6da98393c7f1</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
