<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_Password</name>
   <tag></tag>
   <elementGuidId>4dbb3066-0382-4b68-a5a5-7bccab407760</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='login']/div[2]/div</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value></value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>f6fd22f8-ef59-41e3-9a96-2b5bcd057b33</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>large-6 small-12 columns</value>
      <webElementGuid>bb25dfad-e6c3-448c-a839-d3104dd6d026</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
        Password
        
      </value>
      <webElementGuid>dd65800d-383c-4663-86b3-30687b49e2d4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;login&quot;)/div[@class=&quot;row&quot;]/div[@class=&quot;large-6 small-12 columns&quot;]</value>
      <webElementGuid>9960472d-1d30-4eac-9f35-5aa3f807db7e</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login']/div[2]/div</value>
      <webElementGuid>72259d38-199d-42d5-a604-e549124c92d9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Username'])[1]/following::div[2]</value>
      <webElementGuid>b23f251e-5cdc-4571-9545-0775e6d51811</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='SuperSecretPassword!'])[1]/following::div[4]</value>
      <webElementGuid>38dc477e-8ba6-4357-bd6d-9404d899fec9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Login'])[1]/preceding::div[1]</value>
      <webElementGuid>a6504ebc-fe4f-43eb-884d-97c74da6f045</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//form/div[2]/div</value>
      <webElementGuid>ead6c836-0131-4777-9ed7-a37a4ed381dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[(text() = '
        Password
        
      ' or . = '
        Password
        
      ')]</value>
      <webElementGuid>99e070d3-3574-4eba-bc07-fdde78e8a673</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
