<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>div_A</name>
   <tag></tag>
   <elementGuidId>3a6855c2-df55-4185-a41d-9b0f8a78177f</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='column-a']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#column-a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>div</value>
      <webElementGuid>6c93e6f5-fda4-4aaa-85f5-dda6654ea450</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>column</value>
      <webElementGuid>67bfc460-2c88-4c64-bb88-b19d5954ddd0</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>column-a</value>
      <webElementGuid>24894867-1a14-44bc-80de-91433145344a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>draggable</name>
      <type>Main</type>
      <value>true</value>
      <webElementGuid>08d36560-d6ba-48f2-900a-bf6a3043d1da</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>A</value>
      <webElementGuid>47f89b6d-9ab9-4728-9994-da44065012aa</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;column-a&quot;)</value>
      <webElementGuid>fce4009b-3935-4d81-9f37-07f17b3c56a0</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//div[@id='column-a']</value>
      <webElementGuid>5965fc6f-8dff-468c-a71d-5d48d28e3f9d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='columns']/div</value>
      <webElementGuid>de581f61-0a80-4b04-b2e0-cf52b5c1684c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Drag and Drop'])[1]/following::div[2]</value>
      <webElementGuid>ea8fdb1d-e263-457a-809a-ea6daa0f999c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Elemental Selenium'])[1]/preceding::div[2]</value>
      <webElementGuid>ec8c66cf-b492-4c1a-9af0-35f9c5d71981</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/div/div/div</value>
      <webElementGuid>a7200547-e7ad-4331-9abb-dd6c8d297b0e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//div[@id = 'column-a' and (text() = 'A' or . = 'A')]</value>
      <webElementGuid>24bdc41e-b9e4-4bd6-b242-422173cb8d17</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
