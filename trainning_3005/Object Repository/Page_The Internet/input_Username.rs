<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_Username</name>
   <tag></tag>
   <elementGuidId>a75a48d4-e569-4d3a-a6fa-ceb50c25a5fa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='username']</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>#username</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>75cc4901-b169-494c-a846-7f8ece26fd3c</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>text</value>
      <webElementGuid>4ddf048b-5763-4f7e-be22-d636f59fcd77</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>80c978fd-efa7-4d5e-969e-29fad474c607</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>username</value>
      <webElementGuid>3fa545e3-6652-430a-affb-b1e9d9d33ebd</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;username&quot;)</value>
      <webElementGuid>d35325c4-0ea1-4c6c-9927-05d16795a897</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='username']</value>
      <webElementGuid>c1ac23f6-a658-4ea4-9e4d-eb3e2a06b48b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='login']/div/div/input</value>
      <webElementGuid>1079a765-02ad-4616-8e19-c13e03eb2250</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//input</value>
      <webElementGuid>f56babab-3477-48f0-8140-19ede7603363</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'text' and @name = 'username' and @id = 'username']</value>
      <webElementGuid>a17c08ef-fcc6-4ee3-8d0a-43dd2d14da34</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
